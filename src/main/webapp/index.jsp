<%-- 
    Document   : index
    Created on : Apr 3, 2021, 12:18:16 AM
    Author     : danielmunoz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es-mx">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <title>DEV. EJE01-U1</title>
        <style>
            form {
                /* Centrar el formulario en la página */
                margin: 0 auto;
                width: 400px;
                /* Esquema del formulario */
                padding: 1em;
                border: 1px solid #CCC;
                border-radius: 1em;
            }
        </style>
    </head>
    <body>
        <header align="center">
        </header>
        <section>
            <!-- Estudiante -->
            <div style="text-align: center">
                <h4 style="color:blue"><b>Desarrollo Ejecicio 1 - Unidad 1</b></h4>
            </div>
            <div>
                <form name="FORM-EJE01" method="post">
                    <table>
                        <tr>
                            <th style="text-align: center">
                                <h5 >DATOS ESTUDIANTE IP CIISA</h5>
                            </th>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label>Nombre  </label>
                                    <input 
                                        type="text" 
                                        name="nombre" 
                                        placeholder="Ethan Meadows"
                                        style="WIDTH: 300px; HEIGHT: 15px" 
                                        size=20 
                                        required
                                        autofocus />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label>Indique Sección</label>
                                    <select name="origen"required>
                                        <option value="" disabled selected>Seleccione sección</option>
                                        <option>S-50</option>
                                        <option>S-51</option>
                                        <option>s-52</option>
                                        <option>S-53</option>
                                        <option>S-54</option>
                                        <option>S-55</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <p></p>
                    <!-- BOTON ENVIO -->
                            <center>
                                <button href="index.jsp" 
                                        type="buttom" 
                                        value="summit" 
                                        class="btn btn-link" 
                                        onclick="alert('Datos Enviados Correctamente, GRACIAS!!')">ENVIAR</button>
                            </center>
                    <!-- BOTON ENVIO -->
                </form>
            </div>
            <!-- / COTIZADOR -->
        </section>
        <!-- INFORMACION ALUMNO -->
        <footer>
            <div>
                <br />
                <center>
                    <p>Asignatura        : Taller de Aplicaciones Empresariales - IC201IECIREOL</p>
                    <p>Nombre del Alumno : DANIEL MUÑOZ PASTENE</p>
                    <p>Desarrollo Ver.1.0 April 2021</p>
                    <p>Sección           : 51</p>
                </center>
            </div>
        </footer>
        <!-- INFORMACION ALUMNO -->

    </body>
</html>
